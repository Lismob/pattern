package team;

public class CommandEmailSMS extends Command{
    public CommandEmailSMS(Message message) {
        super(message);
    }
    @Override
    public void execute() {
        getMessage().sendingMessageSMSAndEmail();
    }
}