package team;

public class Main {
    public static void main(String[] args) {
        Message message = new Message();
        AlertCenter alertCenter = new AlertCenter(new CommandSMS(message)
                , new CommandEmail(message)
                , new CommandEmailSMS(message));
        alertCenter.sendingMessageSMS();
        alertCenter.sendingMessageEmail();
        alertCenter.sendingMessageSMSAndEmail();
    }
}
