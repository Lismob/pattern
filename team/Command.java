package team;

abstract public class Command {
    private Message message;

    public Command(Message message) {
        this.message = message;
    }

    abstract public void execute ();

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }
}
