package team;

public class CommandEmail extends Command {
    public CommandEmail(Message message) {
        super(message);
    }
    @Override
    public void execute() {
        getMessage().sendingMessageEmail();
    }
}