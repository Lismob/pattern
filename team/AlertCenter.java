package team;

public class AlertCenter {
    private Command SMS;
    private Command Email;
    private Command EmailAndSMS;

    public AlertCenter(Command SMS, Command email, Command emailAndSMS) {
        this.SMS = SMS;
        Email = email;
        EmailAndSMS = emailAndSMS;
    }
    public void sendingMessageSMS(){
        SMS.execute();
    }
    public void sendingMessageEmail(){
       Email.execute();
    }
    public void sendingMessageSMSAndEmail(){
        EmailAndSMS.execute();
    }

}
