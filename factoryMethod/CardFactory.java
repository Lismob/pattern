package factoryMethod;

public class CardFactory extends Factory {

    @Override
    public Card createCard(String name, int balance) {
        if (name.equals("Credit")) {
            return new CreditCard(balance);
        }
        if (name.equals("Debit")) {
            return new DebitCard(balance);
        }
        throw new IllegalArgumentException();
    }
}
