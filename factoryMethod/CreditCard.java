package factoryMethod;

public class CreditCard implements Card {
   private int balance;
   private String name;

   public CreditCard (int balance){
       this.balance=balance;
       name = "Credit Card";
   }

    public void printCard() {
        System.out.println("Это "+name+" и её баланс: "+balance);
    }
}
