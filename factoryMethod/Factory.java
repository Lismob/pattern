package factoryMethod;

public abstract class Factory {
    public abstract Card createCard(String name, int balance);
}
