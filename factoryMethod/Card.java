package factoryMethod;

public interface Card {
    void printCard();
}
