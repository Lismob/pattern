package factoryMethod;

public class DebitCard implements Card{
    private int balance;
    private String name;


    public DebitCard(int balance) {
        this.balance = balance;
        name = "Debit Card";
    }

    public void printCard() {
        System.out.println("Это "+name+" и её баланс: "+balance);
    }
}
