package factoryMethod;

public class Main {
    public static void main(String[] args) {
        Factory factory=new CardFactory();
        Card card=factory.createCard("Debit" ,100);
        card.printCard();
        Card card2=factory.createCard("Credit",50);
        card2.printCard();
    }
}
