package chainOfResponsibility;

public class Request {
    private String serName;
    private String name;
    private String patronymic;
    private int yearBirth;
    private int number;
    private int month;
    private int incomeData;
    private boolean creditHistory;

    public Request(String serName, String name, String patronymic, int yearBirth, int number, int month, int incomeData, boolean creditHistory) {
        this.serName = serName;
        this.name = name;
        this.patronymic = patronymic;
        this.yearBirth = yearBirth;
        this.number = number;
        this.month = month;
        this.incomeData = incomeData;
        this.creditHistory = creditHistory;
    }

    public String getSerName() {
        return serName;
    }

    public String getName() {
        return name;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public int getYearBirth() {
        return yearBirth;
    }

    public int getNumber() {
        return number;
    }

    public int getMonth() {
        return month;
    }

    public int getIncomeData() {
        return incomeData;
    }

    public boolean getCreditHistory() {
        return creditHistory;
    }

}
