package chainOfResponsibility;

import chainOfResponsibility.applicationProcessor.*;

public class Main {
    public static void main(String[] args) {
        Client client=new Client();
        Bank bank =new Bank();
        Request request=client.sendRequest("Романов","Кирилл"
                ,"Константинови23ч ",1998,21, 5,
                70000, true
                );
        ApplicationProcessor applicationProcessor=new ApplicationProcessorInitials(
                new ApplicationProcessorPersonaInformation(
                        new ApplicationProcessorIncomeData(
                                new ApplicationProcessorCreditHistory(null))
                ));
        applicationProcessor.verification(request);
        bank.crediting(request,applicationProcessor);

    }
}
