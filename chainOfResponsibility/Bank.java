package chainOfResponsibility;

import chainOfResponsibility.applicationProcessor.ApplicationProcessor;

public class Bank {

    public void crediting (Request request, ApplicationProcessor applicationProcessor){
        if (applicationProcessor.getFlag()
                &&request.getIncomeData()>20000
                &&request.getCreditHistory()){
            System.out.println("Кредит одобрен");
        }
        else {
            System.out.println("В кредите отказано");
        }
    }
}
