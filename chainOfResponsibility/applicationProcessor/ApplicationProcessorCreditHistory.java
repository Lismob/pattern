package chainOfResponsibility.applicationProcessor;

import chainOfResponsibility.Request;

public class ApplicationProcessorCreditHistory extends ApplicationProcessor{
    public ApplicationProcessorCreditHistory(ApplicationProcessor applicationProcessor) {
        super(applicationProcessor);
    }

    @Override
    public void verification(Request request) {
        if (!request.getCreditHistory()){
            applicationProcessor.setFlag(false);
            System.out.println("Плохая кредитная история ");
        }
    }
}
