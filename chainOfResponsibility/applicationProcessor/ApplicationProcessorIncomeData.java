package chainOfResponsibility.applicationProcessor;

import chainOfResponsibility.Request;

public class ApplicationProcessorIncomeData extends ApplicationProcessor{
    public ApplicationProcessorIncomeData(ApplicationProcessor applicationProcessor) {
        super(applicationProcessor);
    }

    @Override
    public void verification(Request request) {
        if (request.getIncomeData()>0){
        }
        else {
            applicationProcessor.setFlag(false);
            System.out.println("Произошла ошибка в заполнение данных о доходах");
        }
    }

}
