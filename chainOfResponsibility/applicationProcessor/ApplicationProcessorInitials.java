package chainOfResponsibility.applicationProcessor;

import chainOfResponsibility.Request;

public class ApplicationProcessorInitials extends ApplicationProcessor {
    public ApplicationProcessorInitials(ApplicationProcessor applicationProcessor) {
        super(applicationProcessor);
    }

    @Override
    public void verification(Request request) {
        if (!request.getSerName().matches(".*\\d.*")
                && !request.getSerName().matches(".*\\d.*")
                && !request.getPatronymic().matches(".*\\d.*")) {
            applicationProcessor.verification(request);
        }
        else {
            applicationProcessor.setFlag(false);
            System.out.println("Ошибка в написание ФИО !!!");
        }
    }
}
