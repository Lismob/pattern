package chainOfResponsibility.applicationProcessor;

import chainOfResponsibility.Request;

public class ApplicationProcessorPersonaInformation extends ApplicationProcessor {
    public ApplicationProcessorPersonaInformation(ApplicationProcessor applicationProcessor) {
        super(applicationProcessor);
    }

    @Override
    public void verification(Request request) {
        if (request.getYearBirth() < 2006 && request.getYearBirth() > 1924
                && request.getNumber() <= 31 && request.getNumber() > 0
                && request.getMonth() <= 12 && request.getMonth() > 0) {
            applicationProcessor.verification(request);
        }
        else {
            applicationProcessor.setFlag(false);
            System.out.println("Произошла ошибка в персональных данных");
        }
    }
}
