package chainOfResponsibility.applicationProcessor;

import chainOfResponsibility.Request;

abstract public class ApplicationProcessor {
    private static boolean flag=true;
    protected ApplicationProcessor applicationProcessor;

    public ApplicationProcessor(ApplicationProcessor applicationProcessor) {
        this.applicationProcessor = applicationProcessor;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    public boolean getFlag() {
        return flag;
    }

    abstract public void verification (Request request);
}
