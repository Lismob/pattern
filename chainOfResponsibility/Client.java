package chainOfResponsibility;

public class Client {

    public Request sendRequest(String serName, String name, String patronymic
            , int yearBirth, int number, int month, int incomeData
            , boolean creditHistory) {
        return new Request(serName, name, patronymic, yearBirth, number
                , month, incomeData, creditHistory);
    }

}
